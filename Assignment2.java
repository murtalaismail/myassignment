/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2;

/**
 *
 * @author MURTALA
 */
public class Assignment2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Program to calculate simple interest
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Enter two numbers to perform the operation: ");
        int x = input.nextInt();
        int y = input.nextInt();
        int sum = x+y;
        int difference = x-y;
        int product = x*y;
        double quotient= x/y;
        
        System.out.println("The sum is: "+sum);
        System.out.println("The difference is: "+difference);
        System.out.println("The product is: "+product);
        System.out.println("The quotient is: "+quotient);
        
    }
    
}